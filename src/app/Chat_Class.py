'''
Created on Apr 27, 2020

@author: paullc, gradyorr
URL: https://posthere.io/817e-41d7-9a56
'''
import requests
import tkinter
import sys
import string
import json

class Chat:
    def __init__(self, url, user, rec):
        self.url = "https://posthere.io/817e-41d7-9a56"
        self.username = user
        self.recipient = rec
        self.chatHistory = [
            {'message': 'init', 
            'sender': "init", 
            'recipient': "init"}]
        self.chat_entries = 0

    """
    @param recentMessage: The most recentMessage typed. 
    @return int: 0 -> no command found, 1 -> "clear", 2 -> "quit", 3 -> "update"

    If 1,2, or 3 is returned, do nothing with message, set new_message = None, and clear, quit, or update the gui. 
    If returns 0, call the Chat's sendMessage() method, then set new_message = None
    
    -Pau
    """
    def checkForCommand(self, recentMessage: string) -> int:
        if(recentMessage != None and len(recentMessage) != 0):
            if(recentMessage[0] == "!"):
                if(recentMessage[1:6] == "clear"):
                    return 1
                elif(recentMessage[1:5] == "quit"):
                    return 2
                elif(recentMessage[1:7] == "update"):
                    return 3
            else:
                return 0

    
    """
    @param message: The message to be sent (string)
    @param user: The name of the User (string)
    @param recipient: The name of the recipient (string)

    Sends a message via HTTP to the defined URL
    
    -Pau
    """
    def sendMessage(self, message: string, user: string, recipient: string):
        objectToSend = {'message': message, 'sender': self.username, 'recipient': recipient}
        requests.post(self.url, data=objectToSend)

    """
    Deletes the entire chat history
    
    -Pau
    """
    def deleteChat(self):
        requests.delete(self.url)

    """
    Gets all of the chats in the history of the URL and returns
    a 2-Dimensional array of the messages sent from least recent to most recent. Each
    inner array has the message string at index 0, and the sender string at index 1, and recipient string at index 2,
    and a sender string at index 3.
    
    -Pau
    """
    def getUpdatedChat(self) -> [[]]:
        fullHistory = requests.get(self.url, headers={'Accept': 'application/json'})
        json = fullHistory.json()
        listOfMessages = []
        for object in json:
            listOfMessages.append(
                [object["body"]["message"], object["body"]["sender"], object["body"]["recipient"],  self.getTimeFromJSON(object)])

        listOfMessages.reverse()
        return listOfMessages

    """
    Gets the time stamp from the json parameter and returns
    The time stamp as Military UTC time
    
    -Grady
    """
    def getTimeFromJSON(self, js: json):
        time_index = js['timestamp'].find("T")+1
        time_str = str(js['timestamp'][time_index:time_index+2]) + \
            str(js['timestamp'][time_index+3:time_index+5])
        return time_str
