'''
GUI
@author: erheault
'''
import sys
import tkinter as tk
from app.Chat_Class import Chat

# Creates a GUI and handles messages
class GUI:
    def __init__(self):
        self.url = "https://posthere.io/817e-41d7-9a56"
        self.name = None
        self.receiver = None
        self.new_message = None
        self.firstRun = True
        self.login() # Runs login screan
        self.chat = Chat(self.url, self.name, self.receiver)

        # Initialize Chat window
        self.root = tk.Tk()
        self.root.title(f"{self.format_name(self.name)}" +
            f" Chatting With {self.format_name(self.receiver)}")
        self.root.config(bd=0)
        self.root.geometry("400x400")
        self.root.resizable(width=False, height=False)
        
        # Initialize scroll bar
        self.scrollbar = tk.Scrollbar(self.root)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y, expand=False)
        
        # Initialize listbox to hold messages
        self.mylist = tk.Listbox(self.root, background='lightgray',
                            font=('Helvetica Neue', 10),
                            yscrollcommand=self.scrollbar.set)
        
        # Place list and link scroll bar to list
        self.mylist.place(width=385, height=375)
        self.scrollbar.config(command=self.mylist.yview)

        # Initialize and place text box to enter messages
        self.text_field = tk.Entry(self.root, width=16, font=('Helvetica Neue', 16))
        self.text_field.place(relx=0, rely=1, width=345, anchor=tk.SW)
        
        # Initialize send button and ties it to send messages
        self.button = tk.Button(self.root,
                        text='send',
                        background='white',
                        font=('Helvetica Neue', 10),
                        command=self.send_message)
        self.button.place(relx=0.96, rely=1, anchor=tk.SE)
        self.root.bind("<Return>", self.callback) # Connect enter key to send message

        # Initialize button to update screan
        self.update_button = tk.Button(self.root,
                        text='update',
                        background='white',
                        font=('Helvetica Neue', 10),
                        command=self.update_chat)
        self.update_button.place(relx=0.96, rely=0.93, anchor=tk.SE)

        # Updates the chat on first run
        if (self.firstRun):
            self.firstRun = False
            self.update_chat()
        # End if

        # Run window
        self.root.mainloop()
    """
    Returns the name of the sender
    """
    def get_name(self):
        return self.name
    # End get_name


    """
    Returns the name of the receiver
    """
    def get_receiver(self):
        return self.receiver
    # End get_receiver
    
    """
    Formats string to upper case first letter and
    Lower case rest of string. If empty sets to User
    """
    def format_name(self, name):
        if name != None:
            if (len(name) > 1):
                return name[0].upper() + name[1:].lower()
            elif(len(name) == 1):
                return name.upper()
            else:
                return "User"
    # End format_name
    
    """
    Opens a login screan to get name of messenger and receiver
    """
    def login(self):
        """
        Handle pushing button, sets the name and receiver 
        and deletes the window
        """
        def button_pushed():
            self.name = self.format_name(text_field.get())
            self.receiver = self.format_name(text_field_to.get())
            root.destroy() # Ends login screan
        # End button_pushed
        
        """
        Handle Enter button pressed ->
        call send on enter key
        """
        def callback_one(event):
            button_pushed()
        # End callback_one
        
        # Initialize window
        root = tk.Tk()
        root.title('Chat')
        root.configure(width=200, height=100, background='lightgray')

        # Initialize Name label
        label = tk.Label(root,
                    text="Name:",
                    background='lightgray',
                    font=('Helvetica Neue', 10))
        label.place(relx=0.15, rely=0.15, anchor=tk.CENTER)
        
        # Text field to enter name of sender
        text_field = tk.Entry(root)
        text_field.place(relx=0.6, rely=0.15, anchor=tk.CENTER)
        
        # Text field to enter name receiver
        text_field_to = tk.Entry(root)
        text_field_to.place(relx=0.6, rely=0.5, anchor=tk.CENTER)
        
        # Button to enter names and close window to open chat
        button = tk.Button(root,
                        text='Enter',
                        background='white',
                        font=('Helvetica Neue', 10),
                        command=button_pushed)
        button.place(relx=0.5, rely=0.8, anchor=tk.CENTER)
        
        # Label to label To entry
        label_to = tk.Label(root,
                        text="To:",
                        background='lightgray',
                        font=('Helvetica Neue', 10))
        label_to.place(relx=0.19, rely=0.5, anchor=tk.CENTER)
        root.bind("<Return>", callback_one) # Binds enter key to Enter button

        root.mainloop() # Run window
    # End login
    
    """
    Sends message when Enter key is pushed.
    """
    def callback(self, event):
        self.send_message()
        self.text_field.delete(0,"end")
    # End callback
    
    """
    Clears chat when called
    """
    def clear(self):
        self.mylist.delete(0, tk.END)
    # End clear
    
    """
    Returns the most recent message sent by user
    """
    def get_recent(self):
        return self.new_message
    # End get_recent
    
    '''
    Handles sending chat history
    Clears chat then uses chat class to get history
    Parses history to send back formatted by sender with time stamps
    '''
    def update_chat(self):
        self.clear()
        chat_history = self.chat.getUpdatedChat()
        for message in chat_history:
            if (len(message[0]) == 0):
                pass
            elif (message[0][0] == "!"):
                pass
            elif (message[1] == self.name and
                                message[2] == self.receiver):
                self.i_sent(message[3] + message[0])
            elif (message[1] == self.receiver and
                                message[2] == self.name):
                self.received_message(message[3] + message[0])
    # End update_chat

    """
    Sends message with text from text field when called.
    """
    def send_message(self):
        # Get message
        orig_message = self.text_field.get()
        length = len(orig_message)
        flag = True
        
        # Check if message is a command and labels it if it is a command
        com_check = self.chat.checkForCommand(orig_message)

        # Sends chat to postman
        self.chat.sendMessage(orig_message, self.name, self.receiver)

        # Parse message and executes commands
        if (length == 0):
            flag = False
        elif (com_check == 1):
            self.clear()
            self.chat.deleteChat()
            flag = False
        elif (com_check == 2):
            sys.exit()
        elif(com_check == 3):
            self.update_chat()
            flag = False
        else:
            message = self.name + "> " + orig_message

        LEN = 50 # Character limit to wrap text
        count = 1
        
        # Wrap lines to not go out of screan
        while (length > 0 and flag == True):
            message = message[0:LEN]
            self.mylist.insert(tk.END, message)
            message = orig_message[count*LEN:]
            length -= LEN
            count += 1
        self.text_field.delete(0,"end") # Clear textbox
    # End send_message
    
    """
    Sends message to look like the user sent it
    """
    def i_sent(self, orig_message):
        length = len(orig_message)
        flag = True
        
        # Check if message is command and lebels it if it was
        com_check = self.chat.checkForCommand(orig_message)

        # Parse message and execute commands
        if (length == 0):
            flag = False
        elif (com_check == 1):
            self.clear()
            message = orig_message
        elif (com_check == 2):
            sys.exit()
        elif(com_check == 3):
            self.update_chat()
            flag = False
        else:
            message = f"{orig_message[0:2]}:{orig_message[2:4]} "\
            f"{self.name}> {orig_message[4:]}"
            
        LEN = 50 # Character limit to wrap text
        count = 1

        # Wrap text to character limit LEN
        while (length > 0 and flag == True):
            message = message[0:LEN]
            self.mylist.insert(tk.END, message)
            message = orig_message[count*LEN:]
            length -= LEN
            count += 1
    # End i_sent
    
    """
    Sends messages that are received
    """
    def received_message(self, orig_message):
        length = len(orig_message)
        flag = True
        message = ""

        # Checks if chat is a command and labels it if it is one
        com_check = self.chat.checkForCommand(orig_message)

        # Parse message and excecutes commands
        if (length == 0):
            flag = False
        elif (com_check == 1):
            self.clear()
            message = orig_message
        elif (com_check == 2):
            sys.exit()
        elif(com_check == 3):
            self.update_chat()
            flag = False
        else:
            message = f"{orig_message[0:2]}:{orig_message[2:4]} "\
            f"{self.receiver}> {orig_message[4:]}"

        LEN = 50 # Character limit to wrap text
        count = 1

        # Wrap text around character limit LEN
        while(length > 0 and flag == True):
            message = message[0:LEN]
            self.mylist.insert(tk.END, message)
            message = orig_message[count*LEN:]
            length -= LEN
            count += 1
    # End received_message
# End GUI