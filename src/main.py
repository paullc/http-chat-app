"""

RUN EXAMPLE: "python main.py"
"""

# Import both classes from their respective Python files
from app.Chat_Class import Chat
from app.GUI_Class import GUI

# Define the main function to be run when we execute the Python program
def main():
    GUI()

# Use the special Python variable "__name__" to execute the main function when this file is run.
if __name__ == "__main__":
    main()